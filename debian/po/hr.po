#
msgid ""
msgstr ""
"Project-Id-Version: Debian-installer HR\n"
"Report-Msgid-Bugs-To: tasksel@packages.debian.org\n"
"POT-Creation-Date: 2018-05-23 01:37+0200\n"
"PO-Revision-Date: 2021-04-23 17:22+0200\n"
"Last-Translator: Valentin Vidic <vvidic@debian.org>\n"
"Language-Team: Croatian <lokalizacija@linux.hr>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid "Choose software to install:"
msgstr "Odaberite programe za instalaciju:"

#. Type: multiselect
#. Description
#: ../templates:1001
msgid ""
"At the moment, only the core of the system is installed. To tune the system "
"to your needs, you can choose to install one or more of the following "
"predefined collections of software."
msgstr ""
"Trenutno je instaliran samo osnovni sustav. Kako bi prilagodili instalaciju "
"svojim potrebama, možete odabrati instalaciju sljedećih predodređenih skupina "
"programa."

#. Type: multiselect
#. Description
#: ../templates:2001
msgid ""
"You can choose to install one or more of the following predefined "
"collections of software."
msgstr ""
"Možete odabrati jednu ili više sljedećih predodređenih skupina programa."

#. Type: multiselect
#. Description
#: ../templates:3001
msgid "This can be preseeded to override the default desktop."
msgstr "Pretpostavljeno radno okruženje možete promijeniti putem preeseed postavke."

#. Type: title
#. Description
#: ../templates:4001
msgid "Software selection"
msgstr "Odabir programa"
